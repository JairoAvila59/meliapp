package com.jairoavila.domain.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jairoavila.domain.datasource.LocalDataSource
import com.jairoavila.domain.datasource.RemoteDataSource
import com.jairoavila.domain.local.database.entities.Product
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class SearchRepositoryTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    private lateinit var searchRepository: SearchRepository

    @Mock
    private var remoteDataSource: RemoteDataSource = Mockito.mock(RemoteDataSource::class.java)

    @Mock
    private var localDataSource: LocalDataSource = Mockito.mock(LocalDataSource::class.java)

    @Mock
    private var product: Product = Mockito.mock(Product::class.java)

    @Before
    fun setUp() {
        searchRepository = SearchRepository(remoteDataSource, localDataSource)
    }

    @Test
    fun `when save results in database then return a Long Number`() {
        runBlocking {
            Mockito.`when`(localDataSource.insertProduct(product)).thenAnswer { 1L }
            val result = searchRepository.setProductDB(product)
            Assert.assertNotNull(result)
            Assert.assertEquals(1L, result)
        }
    }
}
