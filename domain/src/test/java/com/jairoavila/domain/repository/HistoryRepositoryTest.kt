package com.jairoavila.domain.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jairoavila.domain.datasource.LocalDataSource
import com.jairoavila.domain.local.database.entities.Product
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class HistoryRepositoryTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    private lateinit var historyRepository: HistoryRepository

    @Mock
    private var localDataSource: LocalDataSource = Mockito.mock(LocalDataSource::class.java)

    @Mock
    private var product: Product = Mockito.mock(Product::class.java)

    @Before
    fun setUp() {
        historyRepository = HistoryRepository(localDataSource)
    }

    @Test
    fun `when a result is received in the localDataSource a list of products is returned`() {
        val listTest = listOf(product)
        runBlocking {
            Mockito.`when`(localDataSource.getProductList()).thenAnswer { listTest }
            val result = historyRepository.getProductsDB()
            Assert.assertNotNull(result)
            Assert.assertEquals(listTest, result)
        }
    }
}
