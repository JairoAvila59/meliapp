package com.jairoavila.domain.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jairoavila.domain.datasource.LocalDataSource
import com.jairoavila.domain.utils.Sites
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class SplashRepositoryTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    private lateinit var splashRepository: SplashRepository

    @Mock
    private var localDataSource: LocalDataSource = Mockito.mock(LocalDataSource::class.java)

    @Mock
    private var sites: Sites = Mockito.mock(Sites::class.java)

    @Before
    fun setUp() {
        splashRepository = SplashRepository(localDataSource)
    }

    @Test
    fun `when a result is stored in the localDataStore a unit value is returned`() {
        runBlocking {
            Mockito.`when`(localDataSource.setSite(sites)).thenAnswer { Unit }
            val result = splashRepository.setSite(sites)
            Assert.assertNotNull(result)
            Assert.assertEquals(Unit, result)
        }
    }
}
