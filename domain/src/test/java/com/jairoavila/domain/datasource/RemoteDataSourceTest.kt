package com.jairoavila.domain.datasource

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jairoavila.domain.remote.MeliApi
import com.jairoavila.domain.remote.model.ProductsResponse
import com.jairoavila.domain.utils.MockWebServerBaseTest
import com.jairoavila.domain.utils.NetworkResponse
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.net.HttpURLConnection

@RunWith(JUnit4::class)
class RemoteDataSourceTest : MockWebServerBaseTest() {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    private lateinit var remoteDataSource: RemoteDataSource

    private lateinit var apiService: MeliApi

    override fun isMockServerEnabled(): Boolean = true

    @Before
    fun start() {
        apiService = provideTestApiService()
        remoteDataSource = RemoteDataSource(apiService)
    }

    @Test
    fun `given response ok when fetching results then return a list with elements`() {
        runBlocking {
            mockHttpResponse("json/products_response_one_item.json", HttpURLConnection.HTTP_OK)
            val apiResponse = remoteDataSource.getSearchItemByQuery("", "", 0)
            assertNotNull(apiResponse)
            assertEquals(
                apiResponse,
                NetworkResponse.Success(ProductsResponse("MLA", "Mock Respuesta", null))
            )
        }
    }

    @Test
    fun `given response failure when fetching results then return exception`() {
        runBlocking {
            mockHttpResponse(500)
            val apiResponse: NetworkResponse<ProductsResponse> =
                remoteDataSource.getSearchItemByQuery("", "", 0)

            assertNotNull(apiResponse)
            val expectedValue = NetworkResponse.Error(Exception("HTTP 500 Server Error"))

            assertEquals(
                expectedValue.error.message,
                (apiResponse as NetworkResponse.Error).error.message
            )
        }
    }
}
