package com.jairoavila.domain.datasource

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jairoavila.domain.local.database.dao.ProductsDao
import com.jairoavila.domain.local.database.entities.Product
import com.jairoavila.domain.local.preference.PreferenceHelper
import com.jairoavila.domain.utils.Sites
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class LocalDataSourceTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    private lateinit var localDataSource: LocalDataSource

    @Mock
    private var productsDao: ProductsDao = Mockito.mock(ProductsDao::class.java)

    @Mock
    private var preferenceHelper: PreferenceHelper = Mockito.mock(PreferenceHelper::class.java)

    @Mock
    private var product: Product = Mockito.mock(Product::class.java)

    @Mock
    private var sites: Sites = Mockito.mock(Sites::class.java)

    @Before
    fun setUp() {
        localDataSource = LocalDataSource(productsDao, preferenceHelper)
    }

    @Test
    fun `when a result is stored in the preferences a unit value is returned`() {
        runBlocking {
            val result = localDataSource.setSite(sites)
            Assert.assertNotNull(result)
            Assert.assertEquals(Unit, result)
        }
    }

    @Test
    fun `when a result is received in the preferences a site key is returned`() {
        val siteTest = "MCO"
        Mockito.`when`(preferenceHelper.getString(PreferenceHelper.SITE_KEY, ""))
            .thenAnswer { siteTest }
        val result = localDataSource.getSite()
        Assert.assertNotNull(result)
        Assert.assertEquals(siteTest, result)
    }

    @Test
    fun `when a result is received in the database a list of products is returned`() {
        val listTest = listOf(product)
        runBlocking {
            Mockito.`when`(productsDao.getProductList()).thenAnswer { listTest }
            val result = localDataSource.getProductList()
            Assert.assertNotNull(result)
            Assert.assertEquals(listTest, result)
        }
    }

    @Test
    fun `when save results in database then return a Long Number`() {
        runBlocking {
            Mockito.`when`(productsDao.insertProduct(product)).thenAnswer { 1L }
            val result = localDataSource.insertProduct(product)
            Assert.assertNotNull(result)
            Assert.assertEquals(1L, result)
        }
    }
}
