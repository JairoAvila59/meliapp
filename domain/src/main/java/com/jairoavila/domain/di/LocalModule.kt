package com.jairoavila.domain.di

import android.app.Application
import androidx.room.Room
import com.jairoavila.domain.local.database.MeliAppDatabase
import com.jairoavila.domain.local.database.dao.ProductsDao
import com.jairoavila.domain.local.preference.EncryptedSharedPreferences
import com.jairoavila.domain.local.preference.PreferenceHelper
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

const val DATABASE_NAME = "meliapp_db"

val localModule = module {
    single { provideEncryptedSharedPreferences(androidApplication()) }
    single { providePreferenceHelper(get()) }
    single { provideRoom(androidApplication()) }
    single { provideProductDao(get()) }
}

fun provideEncryptedSharedPreferences(androidApplication: Application): EncryptedSharedPreferences =
    EncryptedSharedPreferences(androidApplication)

fun providePreferenceHelper(encryptedSharedPreferences: EncryptedSharedPreferences) = PreferenceHelper(encryptedSharedPreferences)

fun provideRoom(application: Application): MeliAppDatabase {
    return Room.databaseBuilder(
        application.applicationContext,
        MeliAppDatabase::class.java,
        DATABASE_NAME
    ).build()
}

fun provideProductDao(meliAppDatabase: MeliAppDatabase): ProductsDao = meliAppDatabase.productsDao()
