package com.jairoavila.domain.di

import com.jairoavila.domain.datasource.LocalDataSource
import com.jairoavila.domain.datasource.RemoteDataSource
import org.koin.dsl.module

val dataSourceModule = module {
    single { LocalDataSource(get(), get()) }
    single { RemoteDataSource(get()) }
}
