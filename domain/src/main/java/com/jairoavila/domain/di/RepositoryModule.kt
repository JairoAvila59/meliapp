package com.jairoavila.domain.di

import com.jairoavila.domain.repository.HistoryRepository
import com.jairoavila.domain.repository.SearchRepository
import com.jairoavila.domain.repository.SplashRepository
import org.koin.dsl.module

val repositoryModule = module {
    single { SplashRepository(get()) }
    single { SearchRepository(get(), get()) }
    single { HistoryRepository(get()) }
}
