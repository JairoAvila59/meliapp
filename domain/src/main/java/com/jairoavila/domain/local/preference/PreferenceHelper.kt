package com.jairoavila.domain.local.preference

import android.content.SharedPreferences

class PreferenceHelper(preferences: EncryptedSharedPreferences) {

    private val sharedPreferences: SharedPreferences = preferences.secureSharedPreferences

    fun putString(tag: String, value: String) =
        sharedPreferences.edit().apply { putString(tag, value) }.apply()

    fun getString(tag: String, defaultValue: String) =
        sharedPreferences.getString(tag, defaultValue)

    companion object {
        const val SITE_KEY = "SITE_KEY"
    }
}
