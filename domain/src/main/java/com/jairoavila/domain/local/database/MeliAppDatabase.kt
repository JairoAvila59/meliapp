package com.jairoavila.domain.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.jairoavila.domain.local.database.converters.Converters
import com.jairoavila.domain.local.database.dao.ProductsDao
import com.jairoavila.domain.local.database.entities.Product

@Database(entities = [Product::class], version = 1)
@TypeConverters(Converters::class)
abstract class MeliAppDatabase : RoomDatabase() {
    abstract fun productsDao(): ProductsDao
}
