package com.jairoavila.domain.local.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.jairoavila.domain.local.database.entities.Product

@Dao
interface ProductsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProduct(product: Product): Long

    @Query("SELECT * FROM Product")
    fun getProductList(): List<Product>
}
