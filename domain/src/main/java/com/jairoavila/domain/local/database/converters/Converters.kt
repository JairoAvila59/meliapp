package com.jairoavila.domain.local.database.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jairoavila.domain.local.database.entities.Product

object Converters {

    @TypeConverter
    @JvmStatic
    fun fromInstallment(value: Product.Installment): String = Gson().toJson(value)

    @TypeConverter
    @JvmStatic
    fun toInstallment(value: String): Product.Installment = Gson().fromJson(value, Product.Installment::class.java)

    @TypeConverter
    @JvmStatic
    fun fromAddress(value: Product.Address): String = Gson().toJson(value)

    @TypeConverter
    @JvmStatic
    fun toAddress(value: String): Product.Address = Gson().fromJson(value, Product.Address::class.java)

    @TypeConverter
    @JvmStatic
    fun fromShipping(value: Product.Shipping): String = Gson().toJson(value)

    @TypeConverter
    @JvmStatic
    fun toShipping(value: String): Product.Shipping = Gson().fromJson(value, Product.Shipping::class.java)

    @TypeConverter
    @JvmStatic
    fun fromAttributeList(value: List<Product.Attribute>): String = Gson().toJson(value)

    @TypeConverter
    @JvmStatic
    fun toAttributeList(value: String): List<Product.Attribute> = Gson().fromJson(value, object : TypeToken<List<Product.Attribute>>() {}.type)
}
