package com.jairoavila.domain.local.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity
data class Product(
    @PrimaryKey var id: String,
    @SerializedName("site_id") var siteId: String,
    var title: String,
    var price: Float,
    @SerializedName("currency_id") var currencyId: String,
    @SerializedName("available_quantity") var availableQuantity: Int,
    @SerializedName("sold_quantity") var soldQuantity: Int,
    var condition: String,
    var permalink: String,
    var thumbnail: String,
    @SerializedName("accepts_mercadopago") var acceptsMercadoPago: Boolean,
    var installments: Installment?,
    var address: Address,
    var shipping: Shipping,
    var attributes: List<Attribute>
) : Serializable {
    data class Installment(
        var quantity: Int,
        var amount: Float,
        var rate: Float
    ) : Serializable

    data class Address(
        @SerializedName("state_name") var stateName: String,
        @SerializedName("city_name") var cityName: String
    ) : Serializable

    data class Shipping(
        @SerializedName("free_shipping") var freeShipping: Boolean
    ) : Serializable

    data class Attribute(
        var name: String,
        @SerializedName("value_name") var valueName: String?
    ) : Serializable
}
