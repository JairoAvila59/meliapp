package com.jairoavila.domain.local.preference

import android.app.Application
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey

class EncryptedSharedPreferences(application: Application) {

    private var masterKey = MasterKey.Builder(application, MasterKey.DEFAULT_MASTER_KEY_ALIAS)
        .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
        .build()

    var secureSharedPreferences: SharedPreferences =
        EncryptedSharedPreferences.create(
            application,
            PREFERENCES_KEY,
            masterKey,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )

    companion object {
        private const val PREFERENCES_KEY = "a7vInvIHG8vSHNs0tDIc6"
    }
}
