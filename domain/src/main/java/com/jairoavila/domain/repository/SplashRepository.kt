package com.jairoavila.domain.repository

import com.jairoavila.domain.datasource.LocalDataSource
import com.jairoavila.domain.utils.Sites

class SplashRepository(private val localDataSource: LocalDataSource) {
    fun setSite(site: Sites) = localDataSource.setSite(site)
}
