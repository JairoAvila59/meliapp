package com.jairoavila.domain.repository

import androidx.lifecycle.LiveData
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.liveData
import com.jairoavila.domain.datasource.LocalDataSource
import com.jairoavila.domain.datasource.ProductsPagingSource
import com.jairoavila.domain.datasource.RemoteDataSource
import com.jairoavila.domain.local.database.entities.Product

class SearchRepository(private val remoteDataSource: RemoteDataSource, private val localDataSource: LocalDataSource) {

    fun getSearchProducts(query: String): LiveData<PagingData<Product>> {
        return Pager(
            config = PagingConfig(
                pageSize = NETWORK_PAGE_SIZE,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { ProductsPagingSource(remoteDataSource, query, localDataSource.getSite()) }
        ).liveData
    }

    suspend fun setProductDB(product: Product): Long = localDataSource.insertProduct(product)

    companion object {
        private const val NETWORK_PAGE_SIZE = 10
    }
}
