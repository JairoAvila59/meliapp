package com.jairoavila.domain.repository

import com.jairoavila.domain.datasource.LocalDataSource
import com.jairoavila.domain.local.database.entities.Product

class HistoryRepository(private val localDataSource: LocalDataSource) {
    suspend fun getProductsDB(): List<Product> = localDataSource.getProductList()
}
