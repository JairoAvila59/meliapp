package com.jairoavila.domain.utils

enum class Sites(val id: String) { COLOMBIA("MCO"), ARGENTINA("MLA"), CHILE("MLC") }
