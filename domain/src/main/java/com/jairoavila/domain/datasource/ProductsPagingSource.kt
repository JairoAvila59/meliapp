package com.jairoavila.domain.datasource

import androidx.paging.PagingSource
import com.jairoavila.domain.local.database.entities.Product
import com.jairoavila.domain.utils.NetworkResponse

class ProductsPagingSource(
    private val remoteDataSource: RemoteDataSource,
    private val query: String,
    private val siteId: String?
) : PagingSource<Int, Product>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Product> {
        val position = params.key ?: STARTING_PAGE_INDEX
        val result = remoteDataSource.getSearchItemByQuery(
            siteId = siteId,
            query = query,
            offset = position
        )
        return when (result) {
            is NetworkResponse.Success -> {
                val products = result.data.results
                LoadResult.Page(
                    data = products!!,
                    prevKey = if (position == STARTING_PAGE_INDEX) null else position - LIMIT_RESULTS,
                    nextKey = if (products.isEmpty()) null else (position + LIMIT_RESULTS)
                )
            }
            is NetworkResponse.Error -> LoadResult.Error(result.error)
        }
    }

    companion object {
        private const val STARTING_PAGE_INDEX = 1
        private const val LIMIT_RESULTS = 10
    }
}
