package com.jairoavila.domain.datasource

import com.jairoavila.domain.local.database.dao.ProductsDao
import com.jairoavila.domain.local.database.entities.Product
import com.jairoavila.domain.local.preference.PreferenceHelper
import com.jairoavila.domain.utils.Sites
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class LocalDataSource(
    private val productsDao: ProductsDao,
    private val preference: PreferenceHelper
) {

    fun setSite(site: Sites) = preference.putString(PreferenceHelper.SITE_KEY, site.id)

    fun getSite(): String? = preference.getString(PreferenceHelper.SITE_KEY, "")

    suspend fun getProductList(): List<Product> =
        withContext(Dispatchers.IO) {
            productsDao.getProductList()
        }

    suspend fun insertProduct(product: Product): Long =
        withContext(Dispatchers.IO) {
            productsDao.insertProduct(product)
        }
}
