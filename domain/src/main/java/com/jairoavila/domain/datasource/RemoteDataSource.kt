package com.jairoavila.domain.datasource

import com.jairoavila.domain.remote.MeliApi
import com.jairoavila.domain.remote.model.ProductsResponse
import com.jairoavila.domain.utils.NetworkResponse
import java.lang.Exception

class RemoteDataSource(private val api: MeliApi) {

    suspend fun getSearchItemByQuery(
        siteId: String?,
        query: String,
        offset: Int
    ): NetworkResponse<ProductsResponse> =
        try {
            val response = api.searchItemByQuery(siteId!!, query, offset)
            NetworkResponse.Success(response)
        } catch (e: Exception) {
            NetworkResponse.Error(e)
        }
}
