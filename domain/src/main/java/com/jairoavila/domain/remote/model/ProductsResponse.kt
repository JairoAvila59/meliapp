package com.jairoavila.domain.remote.model

import com.google.gson.annotations.SerializedName
import com.jairoavila.domain.local.database.entities.Product

data class ProductsResponse(
    @SerializedName("site_id") var siteId: String?,
    var query: String?,
    var results: List<Product>?
)
