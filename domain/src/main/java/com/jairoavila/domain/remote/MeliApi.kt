package com.jairoavila.domain.remote

import com.jairoavila.domain.remote.model.ProductsResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MeliApi {

    @GET("sites/{site_id}/search")
    suspend fun searchItemByQuery(
        @Path("site_id") siteId: String,
        @Query("q") query: String,
        @Query("offset") offset: Int = 0,
        @Query("limit") limit: Int = 10
    ): ProductsResponse
}
