package com.jairoavila.domain.local.database

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.google.gson.Gson
import com.jairoavila.domain.local.database.dao.ProductsDao
import com.jairoavila.domain.local.database.entities.Product
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class RoomDatabaseTest {

    private lateinit var productsDao: ProductsDao
    private lateinit var db: MeliAppDatabase

    @Before
    fun createDb() {
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        db = Room.inMemoryDatabaseBuilder(
            appContext, MeliAppDatabase::class.java
        ).build()
        productsDao = db.productsDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun writeProductAndReadInList() {
        productsDao.insertProduct(productTest)
        val byID = productsDao.getProductList()
        assertEquals(byID[0], productTest)
    }

    companion object {
        private const val productJson =
            "{\"accepts_mercadopago\":true,\"address\":{\"city_name\":\"Suba\",\"state_name\":\"Bogot\u00E1 D.C.\"},\"attributes\":[{\"name\":\"Marca\",\"value_name\":\"Sonivox\"},{\"name\":\"Condici\u00F3n del \u00EDtem\",\"value_name\":\"Nuevo\"},{\"name\":\"Modelo\",\"value_name\":\"VS-TO1410\"}],\"available_quantity\":1,\"condition\":\"new\",\"currency_id\":\"COP\",\"id\":\"MCO578963182\",\"installments\":{\"amount\":2908.33,\"quantity\":12,\"rate\":0.0},\"permalink\":\"https://articulo.mercadolibre.com.co/MCO-578963182-tostador-de-pan-2-rebanadas-acero-inoxidable-electrico-_JM\",\"price\":34900.0,\"shipping\":{\"free_shipping\":false},\"site_id\":\"MCO\",\"sold_quantity\":100,\"thumbnail\":\"http://mco-s1-p.mlstatic.com/609923-MCO43260096671_082020-I.jpg\",\"title\":\"Tostador De Pan 2 Rebanadas Acero Inoxidable El\u00E9ctrico \"}"
        private val productTest = Gson().fromJson(productJson, Product::class.java)
    }
}
