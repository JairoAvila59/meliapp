package com.jairoavila.dashboard.ui.search

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.paging.PagingData
import com.jairoavila.dashboard.utils.TestCoroutineRule
import com.jairoavila.domain.local.database.entities.Product
import com.jairoavila.domain.repository.SearchRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.Mockito.`when` as whenever

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class SearchViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var searchViewModel: SearchViewModel

    @Mock
    private var searchRepository: SearchRepository = mock(SearchRepository::class.java)

    @Mock
    private lateinit var insertDBObserver: Observer<Long>

    @Mock
    private val product: Product = mock(Product::class.java)

    @Mock
    private lateinit var productsObserver: Observer<PagingData<Product>>

    @Before
    fun setUp() {
        searchViewModel = SearchViewModel(searchRepository)
    }

    @Test
    fun `when save results in database then return a Long Number`() {
        testCoroutineRule.runBlockingTest {
            searchViewModel.insertDB.observeForever(insertDBObserver)
            whenever(searchRepository.setProductDB(product)).thenAnswer { 1L }
            searchViewModel.setProductDB(product)
            assertNotNull(searchViewModel.insertDB.value)
            assertEquals(1L, searchViewModel.insertDB.value)
        }
    }

    @After
    fun tearDown() {
        searchViewModel.insertDB.removeObserver(insertDBObserver)
        searchViewModel.products.removeObserver(productsObserver)
    }
}
