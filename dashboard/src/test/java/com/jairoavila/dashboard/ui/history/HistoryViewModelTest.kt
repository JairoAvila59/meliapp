package com.jairoavila.dashboard.ui.history

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.jairoavila.dashboard.utils.TestCoroutineRule
import com.jairoavila.domain.local.database.entities.Product
import com.jairoavila.domain.repository.HistoryRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.Mockito.`when` as whenever

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class HistoryViewModelTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private lateinit var historyViewModel: HistoryViewModel

    @Mock
    private var historyRepository: HistoryRepository = mock(HistoryRepository::class.java)

    @Mock
    private lateinit var productObserver: Observer<List<Product>>

    @Mock
    private val product: Product = mock(Product::class.java)

    @Before
    fun setUp() {
        historyViewModel = HistoryViewModel(historyRepository)
    }

    @Test
    fun `when call results in database then return a product list`() {
        testCoroutineRule.runBlockingTest {
            val productList = listOf(product)
            historyViewModel.products.observeForever(productObserver)
            whenever(historyRepository.getProductsDB()).thenAnswer { productList }
            historyViewModel.getProductsDB()
            assertNotNull(historyViewModel.products.value)
            assertEquals(productList, historyViewModel.products.value)
        }
    }
}
