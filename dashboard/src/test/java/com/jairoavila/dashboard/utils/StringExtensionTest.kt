package com.jairoavila.dashboard.utils

import org.junit.Assert.assertEquals
import org.junit.Test

class StringExtensionTest {

    @Test
    fun `should return the value formatted in cash`() {
        val expected = " $ 150,500"
        val valueDouble = 150500.0

        val testResult1 = valueDouble.formatPriceInCOP()

        assertEquals(testResult1, expected)
    }
}
