package com.jairoavila.dashboard.utils

import java.util.Locale

fun Double.formatPriceInCOP(): String = String.format(Locale.US, "%s $ %,.0f", "", this)
