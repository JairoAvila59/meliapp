package com.jairoavila.dashboard.ui.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.jairoavila.dashboard.utils.SingleLiveEvent
import com.jairoavila.domain.local.database.entities.Product
import com.jairoavila.domain.repository.SearchRepository
import kotlinx.coroutines.launch

class SearchViewModel(private val searchRepository: SearchRepository) : ViewModel() {

    private var productQuery = MutableLiveData<String>()
    private var _insertDB = SingleLiveEvent<Long>()
    val insertDB: LiveData<Long> get() = _insertDB

    val products: LiveData<PagingData<Product>> = Transformations.switchMap(productQuery) {
        searchRepository.getSearchProducts(it).cachedIn(viewModelScope)
    }

    fun getProducts(query: String) = productQuery.postValue(query)

    fun setProductDB(product: Product) = viewModelScope.launch { _insertDB.value = searchRepository.setProductDB(product) }
}
