package com.jairoavila.dashboard.ui.detail

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import coil.api.load
import com.jairoavila.dashboard.databinding.DetailFragmentBinding
import com.jairoavila.dashboard.utils.formatPriceInCOP
import com.jairoavila.domain.local.database.entities.Product

class DetailFragment : Fragment() {

    private var _binding: DetailFragmentBinding? = null
    private val binding get() = _binding!!

    private lateinit var product: Product

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            product = it.getSerializable(PRODUCT_DETAIL) as Product
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DetailFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpView()
        binding.tvDetailName.text = product.title
    }

    private fun setUpView() {
        binding.tvDetailName.text = product.title
        binding.ivThumbnail.load(product.thumbnail)
        binding.tvProductPrice.text = product.price.toDouble().formatPriceInCOP()
        binding.tvProductCity.text = product.address.stateName
        binding.tvFee.text = product.installments?.quantity.toString()
        binding.tvFeePrice.text = product.installments?.amount?.toDouble()?.formatPriceInCOP()
        binding.tvProductCondition.text = product.condition
        binding.tvProductInfo.setOnClickListener {
            val uri = Uri.parse(product.permalink)
            Intent(Intent.ACTION_VIEW, uri).also {
                startActivity(it)
            }
        }
        binding.tvMercadoPago.visibility =
            if (product.acceptsMercadoPago) View.VISIBLE else View.GONE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        const val PRODUCT_DETAIL = "PRODUCT_DETAIL"
    }
}
