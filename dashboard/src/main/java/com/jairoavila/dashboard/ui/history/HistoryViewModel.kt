package com.jairoavila.dashboard.ui.history

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jairoavila.dashboard.utils.SingleLiveEvent
import com.jairoavila.domain.local.database.entities.Product
import com.jairoavila.domain.repository.HistoryRepository
import kotlinx.coroutines.launch

class HistoryViewModel(private val historyRepository: HistoryRepository) : ViewModel() {

    private var _products = SingleLiveEvent<List<Product>>()
    val products: LiveData<List<Product>> get() = _products

    fun getProductsDB() = viewModelScope.launch { _products.value = historyRepository.getProductsDB() }
}
