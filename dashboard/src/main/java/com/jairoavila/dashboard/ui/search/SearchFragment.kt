package com.jairoavila.dashboard.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.jairoavila.dashboard.R
import com.jairoavila.dashboard.databinding.SearchFragmentBinding
import com.jairoavila.dashboard.ui.detail.DetailFragment
import com.jairoavila.domain.local.database.entities.Product
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class SearchFragment : Fragment() {

    private val viewModel: SearchViewModel by viewModel()
    private var _binding: SearchFragmentBinding? = null
    private val binding get() = _binding!!

    private lateinit var mAdapter: SearchAdapter
    private lateinit var mProduct: Product

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = SearchFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpSearchFilter()
        setUpRecyclerView()
        setUpObservers()
    }

    private fun setUpSearchFilter() {
        binding.svProductFilter.apply {
            queryHint = getString(R.string.search_title)
            setIconifiedByDefault(false)
            findViewById<EditText>(androidx.appcompat.R.id.search_src_text).apply {
                setHintTextColor(ContextCompat.getColor(requireContext(), R.color.gray_96))
                setTextColor(ContextCompat.getColor(requireContext(), R.color.black))
            }
            isFocusable = false
            clearFocus()
        }

        binding.svProductFilter.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let { viewModel.getProducts(it) }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean = false
        })
    }

    private fun setUpRecyclerView() {
        mAdapter = SearchAdapter { product ->
            mProduct = product
            viewModel.setProductDB(mProduct)
        }

        binding.rvProducts.apply {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun setUpObservers() {
        viewModel.products.observe(viewLifecycleOwner, {
            lifecycleScope.launch {
                binding.svProductFilter.clearFocus()
                binding.ltSearch.visibility = View.GONE
                binding.rvProducts.visibility = View.VISIBLE
                mAdapter.submitData(it)
            }
        })

        viewModel.insertDB.observe(viewLifecycleOwner, {
            val bundle = bundleOf(DetailFragment.PRODUCT_DETAIL to mProduct)
            findNavController().navigate(R.id.action_searchFragment_to_detailFragment, bundle)
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
