package com.jairoavila.dashboard.ui.history

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.jairoavila.dashboard.databinding.ItemProductCardBinding
import com.jairoavila.dashboard.utils.formatPriceInCOP
import com.jairoavila.domain.local.database.entities.Product

class HistoryAdapter(private val products: List<Product>) :
    RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewBinding =
            ItemProductCardBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(viewBinding)
    }

    override fun getItemCount(): Int = products.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bindData(products[position])

    class ViewHolder(private val viewBinding: ItemProductCardBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bindData(mProduct: Product) {
            viewBinding.ivThumbnail.load(mProduct.thumbnail)
            viewBinding.tvProductName.text = mProduct.title
            viewBinding.tvProductPrice.text = mProduct.price.toDouble().formatPriceInCOP()
            viewBinding.tvFreeShipping.visibility =
                if (mProduct.shipping.freeShipping) View.VISIBLE else View.GONE
        }
    }
}
