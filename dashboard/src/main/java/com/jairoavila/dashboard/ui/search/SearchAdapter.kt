package com.jairoavila.dashboard.ui.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.jairoavila.dashboard.databinding.ItemProductCardBinding
import com.jairoavila.dashboard.utils.formatPriceInCOP
import com.jairoavila.domain.local.database.entities.Product

class SearchAdapter(private val mListener: (Product) -> Unit) :
    PagingDataAdapter<Product, SearchAdapter.SearchViewHolder>(DIFF_PRODUCT) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        val viewBinding =
            ItemProductCardBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SearchViewHolder(viewBinding, mListener)
    }

    private fun getItemByIndex(index: Int) = getItem(index)

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        getItemByIndex(position)?.let { holder.bind(it) }
    }

    class SearchViewHolder(
        private val viewBinding: ItemProductCardBinding,
        private val mListener: (Product) -> Unit
    ) :
        RecyclerView.ViewHolder(viewBinding.root) {
        fun bind(mProduct: Product) {
            viewBinding.ivThumbnail.load(mProduct.thumbnail)
            viewBinding.tvProductName.text = mProduct.title
            viewBinding.tvProductPrice.text = mProduct.price.toDouble().formatPriceInCOP()
            viewBinding.tvFreeShipping.visibility =
                if (mProduct.shipping.freeShipping) View.VISIBLE else View.GONE

            viewBinding.cardProduct.setOnClickListener { mListener.invoke(mProduct) }
        }
    }

    companion object {
        private val DIFF_PRODUCT = object : DiffUtil.ItemCallback<Product>() {
            override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean =
                oldItem.title == newItem.title

            override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean =
                oldItem == newItem
        }
    }
}
