plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
}

android {
    compileSdkVersion(AndroidSdk.compileSdkVersion)
    buildToolsVersion(AndroidSdk.buildToolsVersion)

    defaultConfig {
        minSdkVersion(AndroidSdk.minSdkVersion)
        targetSdkVersion(AndroidSdk.compileSdkVersion)
        multiDexEnabled = true
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }

    buildFeatures {
        viewBinding = true
    }
}

dependencies {
    // Support Libraries
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(AndroidPlugins.kotlinStdLibPlugin)
    implementation(AndroidSupport.appcompatPlugin)
    implementation(AndroidSupport.coreKtxPlugin)

    // UI Libraries
    implementation(UIPlugin.constraintLayoutPlugin)
    implementation(UIPlugin.materialPlugin)

    // Utils Libraries
    implementation(UtilsLibraries.coil)
    api(UtilsLibraries.lottie)

    // Architecture Components Libraries
    implementation(ArchComponentsLibraries.viewmodel)
    implementation(ArchComponentsLibraries.lifecycleExtensions)
    implementation(ArchComponentsLibraries.livedata)
    implementation(ArchComponentsLibraries.navigationFragment)
    implementation(ArchComponentsLibraries.navigationUI)
    // Dependency Injection Library
    implementation(DILibraries.koinViewmodel)
    implementation(DILibraries.koinFragment)
    implementation(DILibraries.koin)
    implementation(DILibraries.koinScope)
    testImplementation(Testing.junit)
    testImplementation(Testing.coreTesting)
    testImplementation(Testing.mockitoCore)
    testImplementation(Testing.mockitoInline)
    testImplementation(Testing.expekt) { exclude(group = "org.jetbrains.kotlin") }
    testImplementation(Testing.coroutinesTesting)
    testImplementation(Testing.coroutinesTesting)
    androidTestImplementation(Testing.testJunit)
    androidTestImplementation(Testing.testEspresso)

    implementation(project(mapOf("path" to ":domain")))
}
