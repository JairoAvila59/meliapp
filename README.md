# Challenge de Mercado Libre para Android: MeliApp

Este repositorio contiene un proyecto Android que da solución al reto propuesto por Mercado Libre para la plataforma Android. En esta documentación se encontrará todo lo relacionado al análisis, diseño y solución propuesto para el desarrollo de esta aplicación móvil.

El objetivo del proyecto es demostrar las mejores prácticas, proporcionar un conjunto de directrices y presentar una arquitectura moderna de aplicaciones Android que sea modular, escalable y mantenible.

![Gif1](images/gif1.gif)

## Table of Contents

- [Análisis](#análisis)
  - [Estructura de la API de Mercado Libre](#estructura-de-la-api-de-mercado-libre)
  - [Diseño de las pantallas](#diseño-de-las-pantallas)
  - [Funcionalidades](#funcionalidades)
- [Arquitectura](#arquitectura)
  - [Módulos](#módulos)
    - [Módulo App](#módulo-app)
    - [Módulo del dashboard](#módulo-del-dashboard)
    - [Módulo de dominio](#módulo-de-dominio)
  - [Componentes de Arquitectura](#componentes-de-arquitectura)
  - [Suite de Firebase](#suite-de-firebase)
- [Stack técnico](#stack-técnico)
  - [Integración continua](#integración-continua)
  - [Análisis de código](#análisis-de-código)
  - [Dependencias](#dependencias)
  - [Dependencias de test](#dependencias-de-test)
  - [Plugins](#plugins)
- [License](#license)

## Análisis

### Estructura de la API de Mercado Libre

Se accedió a la consola de desarrolladores de mercado libre para observar como es la estructura de la API de ítems y búsqueda, encontrando este endpoint que permite traer la información de un producto de un país específico y por paginado:

```
https://api.mercadolibre.com/sites/SITE_ID/search?q=PRODUCT&offset=OFFSET&limit=LIMIT
```

### Diseño de las pantallas

Basándose en el diseño de la aplicación de mercado libre mi propuesta es diseñar una aplicación que comparta los mismos estilos y estructura de los elementos de vista, con esto las pantallas que defino a continuación son las que se realizan en el ejercicio:

- **Pantalla de Splash**: Se mostrará la animación del icono de la aplicación en el centro de la pantalla, luego de determinado tiempo se mostrará una microinteracción para seleccionar el país del usuario.
- **Pantalla de búsqueda de productos**: Se mostrará un buscador la parte superior de la pantalla y una animación en el centro de la pantalla invitando al usuario a realizar una búsqueda. Luego que el usuario realice una búsqueda, se desplegará una lista con todos los elementos encontrados, el usuario podrá hacer scroll para seguir visualizando más elementos.
- **Pantalla de detalle de producto**: El usuario podrá oprimir sobre alguno de los elementos resultado de la búsqueda y se mostrar una pantalla con la información relevante de ese elemento.
- **Pantalla de búsquedas recientes**: Se mostrará una lista de los elementos a los cuales el usuario haya oprimido para ver el detalle.

### Funcionalidades
El objetivo del desarrollo de este ejercicio es implementar una aplicación que sea:

- **Escalable**: Adoptar la suite de Firebase para lograr obtener información del uso de la aplicación que permitan conocer los detalles más importantes que permiten una escalabilidad oportuna.
- **Segura**: Adoptar la guía de seguridad de Android Jetpack para encriptar información sensible
- **Usable**: Adoptar guías de estilo de Material Design para que la aplicación luzca una interfaz moderna.
- **Mantenible**: Adoptar guías de modularización para lograr que la aplicación sea lo menos acoplada posible y se pueda realizar un mantenimiento más efectivo.

## Arquitectura

La arquitectura de la aplicación se basa, aplica y cumple estrictamente con cada uno de los siguientes 5 puntos:

-   [Android architecture components](https://developer.android.com/topic/libraries/architecture/), parte de Android Jetpack para dar a proyectar un diseño robusto, testeable y mantenible.
-   Patron [Model-View-ViewModel](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel) (MVVM) facilita una [separación](https://en.wikipedia.org/wiki/Separation_of_concerns) de desarrollo de la interfaz gráfica de usuario y la logica de negocio.
-   [Modularización](https://proandroiddev.com/build-a-modular-android-app-architecture-25342d99de82) Permite desarrollar características de forma aislada, independientemente de otras características.

### Módulos

Los módulos son una colección de archivos fuente y configuraciones de construcción que permiten dividir un proyecto en unidades discretas de funcionalidad. En este caso además de dividir por funcionalidad/responsabilidad, existe la siguiente dependencia entre ellos:

El gráfico anterior muestra la modularización de la aplicación:
-    `:app` depende de `:dashboard` y `:domain`
-    `:dashboard` depende de `:domain`.
-    `:domain` no tiene dependencia.

![Img1](images/img1.png)

####  Módulo App

El módulo `:app` se encarga de iniciar los gráfico de inyección de dependencias y los recursos de animaciones de lottie diferenciando especialmente entre los distintos entornos de las aplicaciones, contiene la pantalla de Splash y la microinteracción para seleccion de pais.

#### Módulo del dashboard

El módulo `:dashboard` es una biblioteca que se encarga de contener las pantallas de búsqueda, detalle y historial. Además, contiene una actividad que maneja estas vistas mediante un bottom bar navigation y sus respectivos viewmodel para la interacción con la capa de datos.

#### Módulo de dominio

El módulo `:domain`es una biblioteca cuya funcionalidad servir peticiones de red, acceder a la base de datos y preferencias compartidas.  Proporcionando la fuente de datos para las muchas características que lo requieren.

### Componentes de Arquitectura

Idealmente, los ViewModels no deberían saber nada de Android. Esto mejora la testabilidad, la seguridad de las fugas y la modularidad. Los ViewModels tienen alcances diferentes a los de las actividades o fragmentos. Mientras un ViewModel está vivo y funcionando, una actividad puede estar en cualquiera de sus estados del ciclo de vida. Las actividades y los fragmentos pueden ser destruidos y creados de nuevo mientras el ViewModel no es consciente.

Pasar una referencia de la Vista (actividad o fragmento) al ViewModel es un riesgo serio. Supongamos que el ViewModel solicita datos de la red y los datos vuelven algún tiempo después. En ese momento, la referencia de la Vista podría ser destruida o podría ser una actividad antigua que ya no es visible, generando una fuga de memoria y, posiblemente, un crash.

La comunicación entre las diferentes capas sigue el diagrama anterior utilizando el paradigma reactivo, observando los cambios en los componentes sin necesidad de callbacks evitando fugas y casos de borde relacionados con ellos.

También la seguridad es un factor muy importante en cualquier aplicación móvil y es por esta razón que se implementa la libreria de seguridad de Android Jetpack para encriptar las preferencias compartidas donde se almacenan datos que deben persistir en la vida de la aplicación .

### Suite de Firebase

Se implementa este proyecto en Firebase para el manejo principalmente:

- **Rendimiento:** Se utiliza Firebase Perfomance para medir el rendimiento de la aplicación.
![Img2](images/img2.png)
- **Errores y fallos:** Se utiliza Firebase Crashlytics para el monitoreo de errores y fallos.
![Img3](images/img3.png)
- **Analiticas:** Se utiliza Firebase Analytics para visualizar todos los evento marcados que ayuden a entender el comportamiento que tiene la app frente a los usuarios.
![Img4](images/img4.png)
- **Distribucion de aplicación para pruebas:** Se utiliza App Distribution para distribuir el demo de este reto a los ingenieros de mercado libre para su revisión.

## Stack técnico

### Integración continua

Este proyecto está alojado en el repositorio Bitbucket el cual permite realiza un proceso de desarrollo basado en integración continua mediante la ejecución de un pipeline que verifica la compilación correcta de la aplicación antes de ser mezclado a la rama principal.

- **Ramas por funcionalidad o  característica**: Por cada nueva funcionalidad o característica se crea una rama partiendo de master.

```
feature/dashboard-module-creation
```
- **Pull Request**: Luego que se realiza un push de esa rama de característica, se crea un pull request encargado de esperar que los desarrolladores participante aprueben el cambio, este modulo dispara automáticamente el pipeline.
![Img5](images/img5.png)
- **Pipeline**: Verifica la compilación correcta de la aplicación mediante la descarga de gradle y ejecuta las tareas de construcción, ktlint y test.
![Img6](images/img6.png)

### Análisis de código

| Plugin  | Check command
|--------|------------:|---------------|-------------|
| [ktlint](https://github.com/pinterest/ktlint) | `./gradlew ktlint`
| [lint](https://developer.android.com/studio/write/lint) | `./gradlew lint`

### Dependencias

Este proyecto aprovecha muchas bibliotecas, plugins y herramientas populares del ecosistema Android. La mayoría de las bibliotecas están en la versión estable, a menos que haya una buena razón para utilizar la dependencia no estable.

- Kotlin
- Kotlin DSL
- Gradle KTS
-   Jetpack:
    -   Android KTX
    -   AndroidX
    -   View Binding
    -   Lifecycle
    -   LiveData
    -   ViewModel
    -   Security
    -   Room
    -   Paging 3
    -   Navigation
-   Material Design
-   Coroutines
-   Koin
-   Lottie
-   Retrofit2
-   Coil
-   Gson


### Dependencias de test

-   Espresso
-   JUnit
-   Mockito
-   Mock Web Server
-   AndroidX Test

### Plugins

-   Ktlint

### License
```

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.