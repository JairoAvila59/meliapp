package com.jairoavila.meliapp.ui.splash

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jairoavila.domain.repository.SplashRepository
import com.jairoavila.meliapp.utils.MainCoroutineRule
import com.jairoavila.meliapp.utils.getOrAwaitValue
import com.winterbe.expekt.should
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito

@ExperimentalCoroutinesApi
class SplashViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val coroutinesRule = MainCoroutineRule()

    private lateinit var splashViewmodel: SplashViewModel

    @Mock
    val repository: SplashRepository = Mockito.mock(SplashRepository::class.java)

    @Before
    fun onSetup() {
        splashViewmodel = SplashViewModel(repository)
    }

    @Test
    fun `when calling for delay then return state`() {
        val actualResult = splashViewmodel.launchFlowEvent.getOrAwaitValue {
            coroutinesRule.advanceTimeBy(SplashViewModel.SPLASH_DURATION)
        }
        actualResult.should.equal(SplashViewModel.SCREEN_TEST)
    }
}
