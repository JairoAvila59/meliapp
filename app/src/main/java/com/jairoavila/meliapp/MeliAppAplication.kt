package com.jairoavila.meliapp

import android.app.Application
import com.airbnb.lottie.LottieCompositionFactory
import com.jairoavila.domain.di.dataSourceModule
import com.jairoavila.domain.di.localModule
import com.jairoavila.domain.di.networkModule
import com.jairoavila.domain.di.repositoryModule
import com.jairoavila.meliapp.di.viewmodelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MeliAppAplication : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
        initLottie()
    }

    private fun initKoin() = startKoin {
        androidContext(this@MeliAppAplication)
        modules(
            arrayListOf(
                viewmodelModule,
                repositoryModule,
                networkModule,
                localModule,
                dataSourceModule
            )
        )
    }

    private fun initLottie() {
        LottieCompositionFactory.fromRawRes(this, R.raw.shopping_animation)
        LottieCompositionFactory.fromRawRes(this, com.jairoavila.dashboard.R.raw.search)
    }
}
