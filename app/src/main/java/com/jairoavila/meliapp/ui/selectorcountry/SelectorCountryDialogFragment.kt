package com.jairoavila.meliapp.ui.selectorcountry

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.jairoavila.domain.utils.Sites
import com.jairoavila.meliapp.databinding.FragmentSelectorCountryDialogBinding

class SelectorCountryDialogFragment : BottomSheetDialogFragment() {

    private var mListener: ((Sites) -> Unit)? = null
    private var _binding: FragmentSelectorCountryDialogBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSelectorCountryDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding?.cvColombia?.setOnClickListener { sendCountrySelected(Sites.COLOMBIA) }
        _binding?.cvArgentina?.setOnClickListener { sendCountrySelected(Sites.ARGENTINA) }
        _binding?.cvChile?.setOnClickListener { sendCountrySelected(Sites.CHILE) }
    }

    private fun sendCountrySelected(sites: Sites) {
        dismissAllowingStateLoss()
        mListener?.invoke(sites)
    }

    override fun show(manager: FragmentManager, tag: String?) {
        val ft = manager.beginTransaction()
        ft.add(this, tag)
        ft.commitAllowingStateLoss()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {

        @JvmStatic
        fun instance(listener: (Sites) -> Unit): SelectorCountryDialogFragment =
            SelectorCountryDialogFragment().apply {
                mListener = listener
                isCancelable = false
            }
    }
}
