package com.jairoavila.meliapp.ui.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.jairoavila.meliapp.R
import com.jairoavila.meliapp.databinding.SplashFragmentBinding
import com.jairoavila.meliapp.ui.selectorcountry.SelectorCountryDialogFragment
import com.jairoavila.meliapp.ui.splash.SplashViewModel.Companion.SCREEN_TEST
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashFragment : Fragment() {

    private val viewModel: SplashViewModel by viewModel()
    private var _binding: SplashFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = SplashFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpObservers()
    }

    private fun setUpObservers() {
        viewModel.launchFlowEvent.observe(viewLifecycleOwner) { flowName ->
            when (flowName) {
                SCREEN_TEST -> SelectorCountryDialogFragment.instance { countrySelection ->
                    viewModel.setSite(countrySelection)
                    findNavController().navigate(R.id.action_splashFragment_to_dashboardActivity)
                }.show(
                    requireActivity().supportFragmentManager,
                    SelectorCountryDialogFragment::class.java.simpleName
                )
                else -> throw IllegalArgumentException("Undefined fragment id $flowName")
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
