package com.jairoavila.meliapp.ui.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.jairoavila.domain.repository.SplashRepository
import com.jairoavila.domain.utils.Sites
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.onStart

class SplashViewModel(private val repository: SplashRepository) : ViewModel() {

    val launchFlowEvent = flowOf(SCREEN_TEST)
        .onStart { delay(SPLASH_DURATION) }
        .asLiveData()

    fun setSite(site: Sites) = repository.setSite(site)

    companion object {
        const val SCREEN_TEST = "SCREEN_TEST"
        const val SPLASH_DURATION = 5_000L
    }
}
