package com.jairoavila.meliapp.di

import com.jairoavila.dashboard.ui.history.HistoryViewModel
import com.jairoavila.dashboard.ui.search.SearchViewModel
import com.jairoavila.meliapp.ui.splash.SplashViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewmodelModule = module {
    viewModel { SplashViewModel(get()) }
    viewModel { SearchViewModel(get()) }
    viewModel { HistoryViewModel(get()) }
}
