plugins {
    id("com.android.application")
    id("com.google.gms.google-services")
    id("com.google.firebase.crashlytics")
    id("com.google.firebase.firebase-perf")
    kotlin("android")
}

android {
    compileSdkVersion(AndroidSdk.compileSdkVersion)
    buildToolsVersion(AndroidSdk.buildToolsVersion)

    defaultConfig {
        applicationId = "com.jairoavila.meliapp"
        minSdkVersion(AndroidSdk.minSdkVersion)
        targetSdkVersion(AndroidSdk.compileSdkVersion)
        multiDexEnabled = true
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }

    buildFeatures {
        viewBinding = true
    }
}

dependencies {
    // Support Libraries
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(AndroidPlugins.kotlinStdLibPlugin)
    implementation(AndroidSupport.coreKtxPlugin)
    implementation(AndroidSupport.appcompatPlugin)

    // UI Libraries
    implementation(UIPlugin.constraintLayoutPlugin)
    implementation(UIPlugin.materialPlugin)
    implementation(UIPlugin.recyclerViewPlugin)
    implementation(UIPlugin.cardViewPlugin)

    // Architecture Components Libraries
    implementation(ArchComponentsLibraries.viewmodel)
    implementation(ArchComponentsLibraries.lifecycleExtensions)
    implementation(ArchComponentsLibraries.livedata)
    implementation(ArchComponentsLibraries.navigationFragment)
    implementation(ArchComponentsLibraries.navigationUI)

    // Dependency Injection Library
    implementation(DILibraries.koinViewmodel)
    implementation(DILibraries.koinFragment)
    implementation(DILibraries.koin)
    implementation(DILibraries.koinScope)

    // Firebase Libraries
    implementation(platform(FirebasePlugin.firebaseBom))
    implementation(FirebasePlugin.firebaseCrashlytics)
    implementation(FirebasePlugin.firebaseAnalytics)
    implementation(FirebasePlugin.firebasePerformance)

    // Testing Libraries
    testImplementation(Testing.junit)
    testImplementation(Testing.coreTesting)
    testImplementation(Testing.mockitoCore)
    testImplementation(Testing.mockitoInline)
    testImplementation(Testing.expekt) { exclude(group = "org.jetbrains.kotlin") }
    testImplementation(Testing.coroutinesTesting)
    testImplementation(Testing.coroutinesTesting)
    androidTestImplementation(Testing.testJunit)
    androidTestImplementation(Testing.testEspresso)

    implementation(project(mapOf("path" to ":dashboard")))
    implementation(project(mapOf("path" to ":domain")))
}
